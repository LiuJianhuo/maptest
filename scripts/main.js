const map = new BMapGL.Map("mapContainer");
map.centerAndZoom(new BMapGL.Point(...[112, 32]), 15);
map.enableScrollWheelZoom();
// map.addEventListener("update", (e) => {
//   console.log(e);
// });
const gesture = document.querySelector(".gesture");
const result = document.querySelector(".result");

function addChangeEvt(name) {
  const nameMap = {
    center_changed: "oncenter_changed",
  };
  const status = nameMap[name];

  let startInf = null;

  let manual = false;

  let updateTimer;

  map.addEventListener("update", (e) => {
    console.log(e);
    console.log([map.getCenter().lng, map.getCenter().lat], "before");
    if (!e.changedStatus?.[status]) {
      return;
    }
    if (!startInf) {
      startInf = {
        manual,
        center: [map.getCenter().lng, map.getCenter().lat],
        zoom: map.getZoom(),
      };

      result.innerHTML = `
      <div>开始了manual： ${startInf.manual} </div>
    `;
    }
    if (manual && startInf.manual !== manual) {
      startInf.manual = true;
    }

    clearTimeout(updateTimer);
    updateTimer = setTimeout(() => {
      clearTimeout(updateTimer);
      const endInf = {
        ...startInf,
        zoom: map.getZoom(),
        center: [map.getCenter().lng, map.getCenter().lat],
      };

      console.log("endinf", JSON.stringify(endInf));

      result.innerHTML = `
        <div>结束了manual： ${endInf.manual} </div>
      `;
      startInf = null;
    }, 200);
    // console.log([map.getCenter().lng, map.getCenter().lat], "after");
    // console.log(name, manual);
  });

  let pointerupTimer;

  const pointerdownCb = (e) => {
    console.log("end pointdow");
    clearTimeout(pointerupTimer);
    manual = true;
    console.log("pointerdown");
    gesture.innerHTML = `
    <div>pointerdown</div>
  `;
  };

  const pointerupCb = (e) => {
    pointerupTimer = setTimeout(() => {
      manual = false;
      gesture.innerHTML = `
      <div>pointerup</div>
    `;
    }, 500);
    // manual = false;
  };
  map.getContainer().addEventListener("pointerdown", pointerdownCb);
  map.getContainer().addEventListener("pointerup", pointerupCb);
}

addChangeEvt("center_changed");

function zoom() {
  map.centerAndZoom(
    new BMapGL.Point(...[112 + Math.random() * 0.1, 32 + Math.random() * 0.1]),
    20 * Math.random()
  );
}
